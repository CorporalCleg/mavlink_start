from multiprocessing import Process
from time import sleep
import os
from pymavlink import mavutil #mavutil -- many functions to connect and sending command with mavlink


class ARDUPILOT_KRECHET:
    def __call__(self):
        # strt ardupilot sitl
        os.system("/home/dronereal/ap_new/ardupilot/Tools/autotest/sim_vehicle.py -v plane --console -f JSON:127.0.0.1 --add-param-file=/home/dronereal/ap_krechet/matlab_krechet_params -w -m --out=udp:127.0.0.1:14552")
            


class MATLAB_KRECHET:
    def __call__(self):
       # start matlab simulink model 
       os.system("~/./Matlab/bin/matlab -nodisplay -nosplash -nodesktop -r \"run('/home/dronereal/ap_krechet/ardupilot/libraries/SITL/examples/JSON/MATLAB/krechet/start_krechet_simulink.m');exit;\" &")

class SETUP_KRECHET:
    def __call__(self):
        # setup and change mode
        sleep(5)
        print('pymavink')
        drone = mavutil.mavlink_connection("udpin:localhost:14550") #start a connection listening to a UDP port (mavpoxy console similar command)

        drone.wait_heartbeat()
        print("Heartbeat from system (system %u component %u)"%(drone.target_system, drone.target_component))
        sleep(70)
        drone.mav.command_long_send(drone.target_system, drone.target_component, 
                                    mavutil.mavlink.MAV_CMD_DO_SET_MODE,0,1,5,0,0,0,0,0)

        # receive result of a last command (progress : 0 -- meaning that command accepted)
        msg = drone.recv_match(type="COMMAND_ACK",blocking=True)
        print(msg)

        
        # # send command long to arm a drone
        #sleep(10)
        #drone.mav.command_long_send(drone.target_system, drone.target_component, 
        #                            mavutil.mavlink.MAV_CMD_DO_SET_MODE,0,1,15,0,0,0,0,0)

        # receive result of a last command (progress : 0 -- meaning that command accepted)
        #msg = drone.recv_match(type="COMMAND_ACK",blocking=True)
        #print(msg)
            


if __name__ == '__main__':
    ardupilot_krechet = ARDUPILOT_KRECHET()
    matlab_krechet = MATLAB_KRECHET()
    setup_krechet = SETUP_KRECHET()

    process_ardupilot_krechet = Process(target=ardupilot_krechet)
    process_matlab_krechet = Process(target=matlab_krechet)
    process_setup_krechet = Process(target=setup_krechet)

    process_matlab_krechet.start()
    process_ardupilot_krechet.start()
    process_setup_krechet.start()
    
    process_matlab_krechet.join()
    process_ardupilot_krechet.join()
    process_setup_krechet.join()