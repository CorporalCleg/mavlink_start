from pymavlink import mavutil
import time

connection = mavutil.mavlink_connection('udpin:localhost:14551')

connection.wait_heartbeat()
print('Heartbeat from system (system %u component %u)' % (connection.target_system, connection.target_component))



connection.mav.command_long_send(connection.target_system, connection.target_component, 
                                mavutil.mavlink.MAV_CMD_DO_SET_MODE, 0, 1, 4, 0, 0, 0, 0, 0)

msg = connection.recv_match(type='COMMAND_ACK',blocking=True)
print(msg)

time.sleep(1)

connection.mav.command_long_send(connection.target_system, connection.target_component, 
                                mavutil.mavlink.MAV_CMD_COMPONENT_ARM_DISARM, 0, 1, 0, 0, 0, 0, 0, 0)

msg = connection.recv_match(type='COMMAND_ACK',blocking=True)
print(msg)

time.sleep(1)

connection.mav.command_long_send(connection.target_system, connection.target_component, 
                                 mavutil.mavlink.MAV_CMD_NAV_TAKEOFF, 0, 0, 0, 0, 0, 0, 0, 10)

msg = connection.recv_match(type='COMMAND_ACK',blocking=True)
print(msg)