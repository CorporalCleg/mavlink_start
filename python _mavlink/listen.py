from pymavlink import mavutil
import time

connection = mavutil.mavlink_connection('udpin:localhost:14551')

connection.wait_heartbeat()
print('Heartbeat from system (system %u component %u)' % (connection.target_system, connection.target_component))

while True:
    msg = connection.recv_match(type='SIMSTATE',blocking=True)
    print(msg)
    print('\n')
    time.sleep(1)
